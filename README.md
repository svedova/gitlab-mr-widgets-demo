# GitLab MR Widgets Demo

This is a demo of the GitLab [merge request widgets](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html).

Currently this repository demos:

- [Load performance](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html)
- [Browser performance](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html)
- [Status checks](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html)
- [Terraform states](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)

## Repository Structure

Some widgets (such as load and performanc testing) require a server to be deployed to an environment in order to work. This repository uses [Vercel](https://vercel.com) to host our environments.

### Vercel

Vercel provides a free account for proof of concepts. If you want to use this demo project for your own testing, you will need to create an account. You'll also need to set up two CI variables:

- VERCEL_SCOPE: this will be your vercel URL namespace. You can find this in your Vercel account settings.
- VERCEL_TOKEN: This is an access token you can generate in your Vercel account settings.
